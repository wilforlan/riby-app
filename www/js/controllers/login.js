define(['app'],function (app) {
  app.controller('loginCtrl', ['$scope','$ionicPopup', '$http', 'api', function($scope, $ionicPopup,$http, api) {
    $scope.$on("$ionicView.beforeEnter", function(){
      var dm_auth = "xx";
      if (dm_auth) {
        $scope.user = {
          name:dm_auth.phone,
          password:''
        }
      } else {
        $scope.user = {
          name:'',
          password:''
        }
      }
    });

    $scope.login = function(client) {
      $("#login_button").hide();
      $("#loader_bar").show();
      $.ajax({
        method: "POST",
        url: api+"/login/login?key=92215a06d3349532a5669db8dacb1b06",
        data: client,
        contentType: "application/x-www-form-urlencoded",
        success: function(data, status){
          // console.log(data);
          if (data.status == true) {
            var user = data.data;
            $.ajax({
              method: "GET",
              url: api+"/user/getuserdetail?key="+data.data.user_access_token,
              contentType: "application/x-www-form-urlencoded",
              success: function(data, status){
                var loginDetails = {"user":user, "profile":data.data};
                window.localStorage.setItem("loginDetails",JSON.stringify(loginDetails));
                location.href = "#/tab/home";
              },
            });
          }
          else if(data.status == false) {
            $("#login_button").show();
            $("#loader_bar").hide();
            $scope.error_msg = data.message;
            $scope.msg = 'Error'; 
            // console.log(data.message);
            $("#login_error").show();
          }
        },
      error: function(){
        $scope.internal_error = 1;
      }
      })
    };

    $scope.showPassword = function() {
      $scope.show_psd = !$scope.show_psd
    }

  }]);
});
