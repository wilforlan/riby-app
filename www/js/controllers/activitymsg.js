define(['app'],function (app) {
    app.controller('activitymsgCtrl', ['$scope','$ionicPopup', function($scope, $ionicPopup) {
        // Triggered on a button click, or some other target
        $scope.addcomment = function() {
            $scope.data = {}
            var myPopup = $ionicPopup.show({
                // template: '<input type="text" ng-model="data.comment">',
                template: '<div class="row"><div class="col col-33 col-offset-33"><i class="ionicons ion-record" style="font-size: 50px; margin-left:15px;"></i><br/><h6 style="margin-left:15px;">Record</h6></div></div>',
                title: 'Post Comment',
                scope: $scope,
                buttons: [
                    { text: '<i class="ionicons ion-close"></i>' },
                    {
                        text: '<b><i class="ionicons ion-checkmark"></i></b>',
                        type: 'button-positive',
                        onTap: function(e) {
                            return $scope.data.comment;
                        }
                    }
                ]
            });
            myPopup.then(function(res) {
                console.log('Tapped!', res);
            });
        };

    }]);
   
});